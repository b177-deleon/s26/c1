db.fruits.aggregate([
   { $unwind: "$origin" },
   { $match: { onSale: true, origin: "Philippines"} },
   { $group: { _id: "$origin", maxPrice: { $max: "$price" } } }
]);